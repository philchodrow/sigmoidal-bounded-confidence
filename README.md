# Adaptive Opinion Dynamics

This repository contains Julia code for running and analyzing a simple model of opinion dynamics intended to interpolate between consensus-type models and bounded-confidence type models. 
