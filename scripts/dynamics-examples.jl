using Revise
using AdaptiveOpinions
using DataFrames
using CSV

using DifferentialEquations
using RCall
using Roots
using ForwardDiff
using Graphs

### for retrieving solutions

function experiment(γ, δ, x; detailed = false, tspan = 100)
    # initialize topology
    # A, x, z = pathGraph(n, x);
    # containers for dynamical updates
    dx = similar(x)
    W = similar(A)
    # collect parameters
    p = (A, z, β, γ, δ, W)
    # time interval over which to solve
    tspan = (0.0, tspan)
    # solve the ODE
    prob = ODEProblem(dynamics!, x, tspan, p)
    # retrieve the solutions
    sol = solve(prob,  dense=true, dt = 0.5, adaptive = false);

    u, t = sol.u, sol.t

    DF = DataFrame(
        node = repeat(1:length(u[1]), length(t)),
        x = cat(u..., dims = 1), 
        t = repeat(t, inner = length(u[1])),
        gamma = γ
    ) 
    return DF  
end

g = Graphs.SimpleGraphs.smallgraph(:karate)
A = 1.0*Matrix(adjacency_matrix(g))

n = size(A)[1]
z = zeros(n)
z[1] = z[n] = 1.0

### set parameters

β = 1.0
δ = 0.5

### acquire harmonic solution 



x₀ = vcat(-1, zeros(n-2), 1)            # initial condition
# u, t  = experiment(γ, δ, x₀; tspan = 1000) # harmonic solution


DF = vcat([experiment(γ, δ, x₀; tspan = 30) for γ ∈ [0, 2.5]]...)


R"""
library(tidyverse)

df <- $DF %>% 
    mutate(z = node %in% c(1, 34),
           label = paste0("gamma == ",gamma))

zealots <- df %>% 
    filter(z)

persuadables <- df %>% 
    filter(!z)

p <- persuadables %>% 
        ggplot() + 
        aes(x = t, y = x, group = node) + 
        geom_line(size = .5, data = zealots, linetype = "dashed", color = "firebrick") +
        geom_line(size = .2) + 
        facet_wrap(~label, labeller = label_parsed) + 
        theme_bw() + 
        theme(strip.background = element_blank()) + 
        # scale_color_manual(values = c("black", "darkgrey")) + 
        xlab(expression(italic(t))) + 
        ylab(expression(italic(x)[i])) 

ggsave("fig/dynamics-examples.png", width = 5, height = 2.5, dpi = 600)

"""
