# model
using AdaptiveOpinions

include("paired-clique-utils.jl")

# io
using DataFrames
using CSV

# for basins
using Basins, DynamicalSystems, DifferentialEquations

# for finding stationary states
using IntervalRootFinding
using ForwardDiff
using LinearAlgebra

# for nullclines
using Contour

base_dir = "throughput/paired-cliques"

if !isdir(base_dir)
    Base.Filesystem.mkpath(base_dir)
end

for suffix ∈ ["nullclines", "basins", "attractors", "stationary-states"]
    path = base_dir*"/"*suffix
    if isdir(path)
        Base.Filesystem.rm(path, recursive = true)
    end
    Base.Filesystem.mkpath(path)
end


# -----------------------------------------------
# DYNAMICS DEFINITION
# probably can be made a good bit simpler
# -----------------------------------------------

F(X, p, t) = paired_clique_dynamics(X[1], X[2], p[1], p[2]; γ = p[3], δ = p[4])

# -----------------------------------------------
# ATTRACTION BASINS
# -----------------------------------------------

function save_basins(p, ℓ)
    ds = ContinuousDynamicalSystem(F, 2*rand(2).-1, p)
    integ = integrator(ds) 

    xg = -1.1:0.005:1.1
    yg = -1.1:0.005:1.1

    bsn = Basins.basins_map2D(xg, yg, integ)

    # save basins
    DF = DataFrame()

    path = base_dir*"/basins/run-$ℓ.csv"

    for i ∈ 1:size(bsn.basin)[1]
        df = DataFrame(
            x1 = xg[i],
            x2 = yg,
            attractor = bsn.basin[i,:],
            di = p[1], 
            gamma = p[3], 
            delta = p[4], 
            run = ℓ
        )
        DF = vcat(DF, df)
    end
    CSV.write(path, DF)

    # save attractors
    DF = DataFrame()
    path = base_dir*"/attractors/run-$ℓ.csv"

    for (index, point) ∈ bsn.attractors
        df = DataFrame(
            x1 = 0.5*(point.data[1][1] + point.data[2][1]),
            x2 = 0.5*(point.data[1][2] + point.data[2][2]),
            attractor = index,
            run = ℓ
        )
        DF = vcat(DF, df)
    end
    CSV.write(path, DF)
end


# -----------------------------------------------
# STATIONARY STATES + STABILITY
# -----------------------------------------------


function save_stationary_states(p, ℓ)

    Xg = -1..1

    stationary = roots(x -> F(x, p, 0), Xg × Xg)
    J = x -> ForwardDiff.jacobian(s -> F(s, p, 0), x)
    DF = DataFrame()

    path = base_dir*"/stationary-states/run-$ℓ.csv"

    for root ∈ stationary
        x₁ = (root.interval[1].hi + root.interval[1].lo)/2
        x₂ = (root.interval[2].hi + root.interval[2].lo)/2
        j = J([x₁, x₂])
        df = DataFrame(
            x1 = x₁, 
            x2 = x₂, 
            stable = all(real.(eigen(j).values) .<= 0), 
            gamma = p[3], 
            delta = p[4], 
            di = p[1],
            run = ℓ)
        DF = vcat(DF, df)
    end

    CSV.write(path, DF)
end


# -----------------------------------------------
# NULLCLINES
# -----------------------------------------------



function save_nullclines(p, ℓ)
    DF = DataFrame()

    X = -1.1:0.001:1.1
    Y = -1.1:0.001:1.1

    path = base_dir*"/nullclines/run-$ℓ.csv"

    j = 1

    for i in 1:2

        z = [F([x,y], p, 0)[i] for x in X, y in Y]

        c = Contour.contour(X, Y, z, 0)
        
        for line in c.lines
            xs, ys = Contour.coordinates(line)
            df = DataFrame(
                x1 = xs, 
                x2 = ys, 
                gamma = p[3], 
                delta = p[4], 
                di = p[1], 
                i = i, 
                group = j,
                run = ℓ)
            DF = vcat(DF, df)
            j += 1
        end
    end

    CSV.write(path, DF)
end

conditions = [
    [5, 5, 1.8, 1.25],
    [5, 5, 6  , 1.0 ],
    [5, 5, 14 , 2   ],
    [9, 1, 1.8, 1.25],
    [9, 1, 6  , 1.0 ],
    [9, 1, 14 , 2   ],
]

Threads.@threads for ℓ ∈ 1:length(conditions)
    p = conditions[ℓ]
    save_basins(p, ℓ)
    save_stationary_states(p, ℓ)
    save_nullclines(p, ℓ)
end