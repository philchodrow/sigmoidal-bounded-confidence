using Revise
using AdaptiveOpinions 
using LinearAlgebra
using DataFrames
using DifferentialEquations
using Plots
using Graphs
using RCall
"""
would like to figure out whether the harmonic solution is **always** a stationary point of the dynamics. Looks like no, there is a fixed point that varies continuously with γ such that the harmonic solution is the associated curve when γ = 0. 
"""

g = Graphs.SimpleGraphs.smallgraph(:karate)

A = 1.0*Matrix(adjacency_matrix(g))

n = size(A)[1]

z = zeros(n)
z[1] = z[n] = 1.0

β = 1.0
δ = 0.5

function experiment(γ, x; detailed = false)
    # initialize topology
    # A, x, z = pathGraph(n, x);
    # containers for dynamical updates
    dx = similar(x)
    W = similar(A)
    # collect parameters
    p = (A, z, β, γ, δ, W)
    # time interval over which to solve
    tspan = (0.0, 100.0)
    # solve the ODE
    prob = ODEProblem(dynamics!, x, tspan, p)
    # retrieve the solutions
    sol = solve(prob);
    if detailed
        return sol.u[end], Matrix(analyticJacobian(sol.u[end], p)) 
    end
    return sol[end]
end

function run_experiments(Γ)
    x = zeros(n)
    x[1] =  1.0
    x[n] = -1.0

    m = length(Γ)

    # x = experiment(0.0, x; detailed = false)
    X = zeros(m, n)

    for i ∈ 1:m
        x = experiment(Γ[i], x; detailed = false)
        X[i,:] = x
    end
    return X
end

Γ = 0.0:0.005:2.5
X = run_experiments(Γ)

R"""
library(tidyverse)
df <- as_tibble($X) %>%
    mutate(gamma = $Γ) %>% 
    pivot_longer(-gamma, names_to = "name", values_to = "x") %>% 
    mutate(highlight = (x == 1) | (x == -1))

zealots <- df %>% 
    filter(highlight)

persuadables <- df %>% 
    filter(!highlight)

p <- persuadables %>% 
    ggplot() + 
    aes(x = gamma, y = x, group = name) + 
    geom_line(color = "darkslategrey") +
    geom_line(color = "firebrick", linetype = "dashed", data = zealots) +
    theme_bw() + 
    guides(color = "none") + 
    theme(panel.grid.minor = element_blank()) + 
    ylab("Opinion state") + 
    xlab(expression(italic(γ))) + 
    theme(panel.border = element_blank(), 
    axis.line = element_line(size = 0.5, color = "darkgrey"),
    axis.ticks = element_line(color = "darkgrey"))

ggsave("fig/karate-small-gamma.png", p, width =4, height = 3, dpi = 600)
"""


