"""
Simple tests for a toy model of opinion dynamics, written to generalize several aspects of the bounded-confidence model, particularly those related to the link function. At the moment the behavior is pretty boring, and experiments seem to suggest a unique fixed point. Further experiments or modifications may surface something more interesting. It may also be a feature of the very boring graph topology (effectively a ring) on which we are working.
"""

using Revise
using AdaptiveOpinions # custom package. Requires pkg> activate .
using LightGraphs
using GraphPlot
using Colors
using StatsBase
using Plots
using LaTeXStrings
using Compose
using Cairo
using Fontconfig
using FiniteDiff
using LinearAlgebra

n_colors = 30
color_pal = range(colorant"lightseagreen", stop=colorant"orange", length=n_colors)
edge_pal = range(colorant"white", stop=colorant"gray", length=n_colors)

# G = graphfamous("karate")
G = graphfamous("polbooks")
n = nv(G)
locs_x, locs_y = spring_layout(G)

λ = 0.5
γ = 3.3
η = 0.01
β = 1.0

S = randomInitialization(G, 1)
for a in S.agents a.λ = λ end
for i in vertices(G) S.agents[i].x = i < 50 ? [1.0] : [-1.0] end

f(x1, x2) = logistic_sigmoid(x1, x2; β = β, γ = γ, δ = 0.5)
g(x) = x .+ η .* x .* (1 .- x.^2)

for i = 1:5000 evolve!(S, f, g) end

X = [a.x[1] for a in S.agents]
X = round.(X, digits = 8)

node_colors = [color_pal[trunc(Int, 1.0+(n_colors-1.0)*(x[1]+1.0)/2.0)] for x in X]



# would be nice to figure out how to color the edges here.

edge_weights = repeat([0.0], ne(G))
E = collect(edges(G))
for i in 1:length(E)
      e = E[i]
      edge_weights[i] = f(S.agents[src(e)].x, S.agents[dst(e)].x)
end

edge_colors = [edge_pal[trunc(Int, 1+(n_colors-1)*(w))] for w in edge_weights]

p = gplot(S.G, locs_x, locs_y,
      nodefillc=node_colors,
      # edgestrokec = edge_colors,
      edgelinewidth = .5*edge_weights .+ 0.1)

p

plot(sort(X), seriestype = :scatter)
ylims!(-1, 1)

# draw(PNG("opinion_dynamics/fig/kclub_example_not_aligned.png", 16cm, 16cm), p)

# draw(PNG("opinion_dynamics/fig/kclub_example_bifurcated_graph.png", 16cm, 16cm), p)

# Might be good at this point to test a formula for the Jacobian.
# Simplest method is probably to get it via finite differences.

function F(x, f, g)
      S = randomInitialization(G, 1)
      for i in 1:length(S.agents)
            S.agents[i].x[1] = x[i]
      end
      evolve!(S, f, g)
      return [a.x[1] for a in S.agents]
end

function numericalJacobian(X)
      return FiniteDiff.finite_difference_jacobian(X -> F(X, f, g), X)
end


# What we'd like to do next is to compute the largest eigenvalue (in magnitude) of J as a function of gamma

function F(x, γ, η)
      f(x1, x2) = logistic_sigmoid(x1, x2; β = 1.0, γ = γ, δ = 0.5)
      g(x) = x .+ η .* x .* (1 .- x.^2)
      S = randomInitialization(G, 1)
      for i in 1:length(S.agents)
            S.agents[i].x[1] = x[i]
      end
      evolve!(S, f, g)
      return [a.x[1] for a in S.agents]
end

function numericalJacobian(x₀, γ, η)
      return FiniteDiff.finite_difference_jacobian(x -> F(x, γ, η), x₀)
end

function largest_eig(x₀, γ, η)
      J = numericalJacobian(x₀, γ, η)
      return maximum(abs.(eigvals(J)))
end

largest_eig(X, 1.0, .5)


Y = zero(X)
Y[X .> 0] .= mean(X[X.>0])
Y[X .< 0] .= mean(X[X.<0])

Γ = 0:0.1:5
v = [largest_eig(Y, γ, η) for γ ∈ Γ ]
q = plot(Γ, repeat([1.0], length(Γ)), color = "black", label = "")
plot!(Γ, v, seriestype = :scatter, label = "")
xlabel!(L"\gamma")
ylabel!(L"\max_j  |\nu_j|")

plot!(dpi=300)
display(q)
# savefig("opinion_dynamics/fig/kclub_example_bifurcated.png")

# savefig("opinion_dynamics/fig/kclub_example_not_aligned_stability.png")


for i = 1:1000 evolve!(S, f, g) end
X = [a.x[1] for a in S.agents]
X = round.(X, digits = 8)

maximum(X[X.>0]) - minimum(X[X.>0])
