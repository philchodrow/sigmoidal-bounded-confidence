module AdaptiveOpinions
using ForwardDiff
using LinearAlgebra
using Random
using Parameters

using Random
using Parameters
using FiniteDiff

using Graphs

include("model.jl")
include("mean_field.jl")
include("continuous_time.jl")
include("initial_condition.jl")

export randomInitialization
export evolve!
export evolve_to_convergence!
export logistic_sigmoid
export stateVector
export numericalJacobian
export MF_approx
export meanFieldState
export meanFieldRuns
export trueModelRuns
export weightMatrix
export dynamics!
export dynamics
export autoJacobian
export analyticJacobian
export linkedCompleteGraphs
export pathGraph
export completeGraph
export pairedCliques
end # module
