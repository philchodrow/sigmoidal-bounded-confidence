

######
# Data structure: node list 1:n
# Nodes have opinion vectors and potentially additional parameters
# The model itself should store a graph
# we act on the model via evolution operators with parameters like α
######

mutable struct agent
    """
    The model's representation of an individual agent, or node. Currently has only an opinion, but we are likely to want a receptivity parameter in the future.
    """
    x::Float64
    λ::Float64
    neighbors::Array{Int64}
end

mutable struct opinion_state
    """
    The model's representation of a complete state, including both agents and their connectivity.
    """
    agents::Vector{agent} # agents should be labeled 1:n
    G::Graph        # must contain n nodes
end

function Base.getindex(S::opinion_state, id::Int)
	S.agents[id]
end


function randomOpinionInitialization(n::Int64)
    """
    Initialize a set of agents with random opinions, on a graph.
    """
    agents = Vector{agent}(undef, n)
    for i = 1:n
        x = rand(Float64, 1)[1]
        x = 2*(x - 0.5)
        agents[i] = agent(x, 0.5, [])
    end
    return agents
end

function randomInitialization(G::Graph)
    """
    Initialize
    """
    n = nv(G)
    agents = randomOpinionInitialization(n)
    
    for i ∈ 1:n
        agents[i].neighbors = neighbors(G, i)
    end
    
    return opinion_state(agents,G)
end

function evolve!(S::opinion_state, f, g = identity)
    """
    Following Heather's paper, we are going to do synchronous updates for now, might want to consider asynchronous at some future time, could be interesting.
    """
    n = length(S.agents)      # number of agents

    X = Vector{Float64}(undef, n)

    for i ∈ 1:n
        # form new opinion: weighted average of neighbor's opinions
        N = S[i].neighbors
        @inbounds w = [f(S[i].x, S[j].x) for j in N]
        w = w/sum(w)
        @inbounds x_ = sum(w[j]*S[N[j]].x for j in 1:length(w))

        # update opinion
        λ = S[i].λ
        X[i] = λ*g(S[i].x) + (1-λ)*x_
    end
    for i in 1:n
        @inbounds S[i].x = X[i]
    end
end

function euc_dist(x_1, x_2)
    return (x_1 - x_2)^2
end

function logistic_sigmoid(x; β=1, γ=1, δ = 0.5)
     β/(1 + exp(γ*(x - δ)))
end

function logistic_sigmoid(x_1, x_2; β=1, γ=1, δ=0.5)
    """
    Logistic sigmoid function of the euclidean distance between opinions. We can think of this as a smooth approximation to the standard bounded-confidence binary thresholding.
    β controls maximum value, with value of 2 giving a maximum function value of 1
    γ controls the steepness of the curve
    δ controls the location of the midpoint
    """
    return logistic_sigmoid(euc_dist(x_1, x_2); β=β, γ=γ, δ=δ)
end

function stateVector(S::opinion_state)
    x = [a.x for a in S.agents]
    return x
end

function weightMatrix(S::AdaptiveOpinions.opinion_state; return_deriv = false, β = β, γ = γ, δ = δ)
    x = stateVector(S)
    dists =  (x .- x') .^ 2
    n = length(x)
    
    W = logistic_sigmoid.(dists; β = β, γ = γ, δ = δ) 
    W = W .* Matrix(adjacency_matrix(S.G))

    if return_deriv
        ∂W = 2γ*W.*(1 .- W).*(x .- x')
        return W, ∂W
    end

    return W
end




################################################################################
# JACOBIANS
################################################################################

function numericalJacobian(S::opinion_state, x::Vector{Float64}, f, g)
    """
    need to incorporate λ in here as well, I think. 
    """
    function F(x, f, g)
          for i in 1:length(S.agents)
                S[i].x = x[i]
          end
          evolve!(S, f, g)
          return stateVector(S)
    end
    return FiniteDiff.finite_difference_jacobian(x -> F(x, f, g), x)
end



